/**
 * Transmitter / Receiver SIM-GPS
 * @author MIGUEL ANGEL FRAUSTO ROJAS
 * @author OSCAR DANIEL FRAUSTO ROJAS
 *
 * PROGRAM BASED AND INSPIRED FROM:
 *
 * SIM GPS Transmitter from CheapskateProjects
 * https://github.com/CheapskateProjects/SimGpsTransmitter
 */

#include <SoftwareSerial.h>
#include <TinyGPS++.h>

// APN Constants
static String apn="internet.movistar.mx";
static String apn_user = "movistar";
static String apn_pass = "movistar";

// SERVER Constants
static String loggingPassword="qwerty";
static String serverIP="gps.frausto.mx";

// GPS And SIM Pins
static const int SimRXPin = 7, SimTXPin = 6;
static const int GPSRXPin = 8, GPSTXPin = 9;

// Baud Rates
static const uint32_t SimBaudrate = 9600;
static const uint32_t GPSBaud = 9600;
static const uint32_t SerialBaudrate = 9600;

// Software Serial pin init
SoftwareSerial sim_ss(SimRXPin, SimTXPin);
SoftwareSerial gps_ss(GPSRXPin, GPSTXPin);

// TinyGPSPlus Object
TinyGPSPlus gps;

// Helper vars
unsigned long previous=0;
unsigned long beltTime;
boolean httpInit = false;
String responseString;

/**
 * Print some message in Serial port and delay
 * @param msg Message to print
 * @param dTime Time to delay
 */
void printAndDelay(String msg, int dTime) {
  Serial.println(msg);
  delay(dTime);
}

/**
 * Print a character in Serial and delay
 * @param c Char to print
 * @param dTime Time to delay
 */
void serialWriteChar(char c) {
  Serial.write(c);
}

/**
 * Print SIM output
 */
void runsl(int dTime) {
  while (sim_ss.available()) {
    serialWriteChar(sim_ss.read());
  }
}

/**
 * Execute AT command
 * @param command Command to excecute
 */
void executeAT(String command, int dTime) {
  sim_ss.println(command);
  runsl(dTime);
  //Delay once full output printed
  delay(dTime);
}

void setup() {
  // TODO: Set pins for gas interrupt
  // Initialize status pins
  //pinMode(ErrorPin, OUTPUT);
  //pinMode(SimConnectionPin, OUTPUT);
  //digitalWrite(ErrorPin, LOW);
  //digitalWrite(SimConnectionPin, LOW);

  Serial.begin(SerialBaudrate);
  sim_ss.begin(SimBaudrate);
  gps_ss.begin(GPSBaud);
  sim_ss.listen();

  printAndDelay("Waiting for init", 5000);
  printAndDelay("Init... waiting until module has connected to network", 0);

  // Start AT communication.
  executeAT("AT", 100);
  //Set full mode
  executeAT("AT+CFUN=1", 100);
  //Set APN
  executeAT("AT+SAPBR=3,1,APN," + apn, 100);
  //Set user
  executeAT("AT+SAPBR=3,1,USER," + apn_user, 100);
  //Set password
  executeAT("AT+SAPBR=3,1,PWD," + apn_pass, 100);
  //Verify connection
  executeAT("AT+SAPBR=1,1", 100);
  executeAT("AT+SAPBR=2,1", 2000);
  //Initialize http
  executeAT("AT+HTTPINIT", 100);
  httpInit = true;

  gps_ss.listen();
  previous = millis();
  printAndDelay("starting loop!", 0);
}

String buildPostBody(String lat, String lon, String vel, String sat) {
  String body = "{";
  //Add longitude
  body += "\"long\":";
  body += "\""+ lon +"\",";
  //Add latitude
  body += "\"lat\":";
  body += "\""+ lat +"\",";
  //Add speed
  body += "\"velocidad\":";
  body += "\""+ vel +"\",";
  //Add satellites
  body += "\"satelites\":";
  body += "\""+ sat +"\"";
  body += "}";

  return body;
}

void logInfo() {
  // Causes us to wait until we have satelite fix
  if(!gps.location.isValid()) {
    Serial.println("Not a valid location. Waiting for satelite data.");
    return;
  }

  String bodyPOST = buildPostBody(String(gps.location.lat(), 8), String(gps.location.lng(), 8), String(gps.speed.kmph(), 2), String(gps.satellites.value(), DEC));

  printAndDelay("OBTENIDOS DATOS DESDE GPS!.", 100);

  printAndDelay("cadena construida para post:", 100);
  Serial.println(bodyPOST);
  delay(5000);

  sim_ss.listen();
  previous = millis();

  if(!httpInit){
    executeAT("AT+HTTPINIT", 100);
  }

  String url = "AT+HTTPPARA=\"URL\",\"http://";
  url += serverIP;
  url += "/api/logs\"";
  executeAT(url, 100);
  executeAT("AT+HTTPPARA=CONTENT,application/json", 100);
  executeAT("AT+HTTPDATA=160,10000", 100);
  executeAT(bodyPOST, 10000);
  //digitalWrite(SimConnectionPin, LOW);
  executeAT("AT+HTTPACTION=1", 5000); //Perform POST action
  executeAT("AT+HTTPREAD", 100);
  executeAT("AT+HTTPTERM", 100);
  //digitalWrite(SimConnectionPin, HIGH);
  httpInit = false;
  gps_ss.listen();
}

void loop() {
  // If we have data, decode and log the data
  while (gps_ss.available() > 0)
   if (gps.encode(gps_ss.read()))
    logInfo();

  // Test that we have had something from GPS module within first 10 seconds
  if (millis() - previous > 10000 && gps.charsProcessed() < 10) {
    Serial.println("GPS wiring error!");
    while(true);
  }
}
